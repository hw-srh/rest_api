# How to use the api documentation

You can use any openapi 3.0 UI to view the api for jetbrain IDEs use
[the extension from JetBrains](https://plugins.jetbrains.com/plugin/14394-openapi-specifications)
to view and edit the documentation. Or you can use the swagger web editor [here](https://editor.swagger.io/).